package com.dbadmaev.tschool.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OptionsDto implements Serializable {
    private long id;

    private String optionType;

    private String name;

    private BigDecimal price;

    private BigDecimal connectionCost;

    private List<Long> incompatibleOptionsDtoList;

    private List<Long> linkedOptionsDtoList;
}
