package com.dbadmaev.tschool.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TariffsDto implements Serializable {

    private long id;

    private String name;

    private BigDecimal price;

    private int status;
    private List<OptionsDto> possibleOptionsDtoList;
}
