package com.dbadmaev.tschool.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TariffsWithOptionsDto implements Serializable {
    private TariffsDto tariffsDto;
    private List<OptionsDto> OptionsDto;
}
