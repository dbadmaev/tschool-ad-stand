package com.dbadmaev.tschool.service;

import com.dbadmaev.tschool.dto.TariffsWithOptionsDto;

import java.util.List;

public interface StandService {
    List<TariffsWithOptionsDto> getStand();
}
