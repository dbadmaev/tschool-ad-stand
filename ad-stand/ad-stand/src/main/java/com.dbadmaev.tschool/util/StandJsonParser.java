package com.dbadmaev.tschool.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.dbadmaev.tschool.dto.TariffsWithOptionsDto;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Singleton
@Slf4j
public class StandJsonParser {

    private final Gson gson = new Gson();

    public List<TariffsWithOptionsDto> parse(String json) {
        Type tariffsWithOptions = new TypeToken<ArrayList<TariffsWithOptionsDto>>() {
        }.getType();
        List<TariffsWithOptionsDto> tariffsWithOptionsDto = null;

        tariffsWithOptionsDto = gson.fromJson(json, tariffsWithOptions);
        return tariffsWithOptionsDto;
    }
}