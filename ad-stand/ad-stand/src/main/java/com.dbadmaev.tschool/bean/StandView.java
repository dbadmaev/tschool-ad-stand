package com.dbadmaev.tschool.bean;

import com.dbadmaev.tschool.dto.TariffsWithOptionsDto;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import com.dbadmaev.tschool.service.StandService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Slf4j
@Named
@ApplicationScoped
@Getter
public class StandView implements Serializable {

    @Inject
    private StandService standService;

    @Inject
    private ReceiverConfigBean receiverConfigBean;

    @Inject
    @Push(channel = "stand")
    private PushContext context;

    private List<TariffsWithOptionsDto> tariffsWithOptions;

    @PostConstruct
    public void init() {
        log.info("init connection");
        receiverConfigBean.openConnection();
        tariffsWithOptions = standService.getStand();
        log.info(tariffsWithOptions.toString());
    }

    public void updateStand() {
        log.info("Update...");
        tariffsWithOptions = standService.getStand();
        log.info(tariffsWithOptions.toString());
        context.send("update");
        log.info("Update end");
    }
}
